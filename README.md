# Meet Centralway Task - Backend (14-Nov-2017)

We want to create a simple phonebook application where our users can create contacts and save associated phone numbers. The service should be as accessible to as many existing tools as possible.
#### Business Rules:
- [x] One contact can have 0 or more phone numbers
- [x] More than one contact can have the same phone number (think household landlines)
- [x] We support all international phone number formats.

* The full API specification can be found on [Apiary](http://docs.cwcontacts.apiary.io/#)

- - - -

### Technology Stack:
This project has been built using the following:
* Debian as OS.
* Sqlite 3
* Python 3.6
* Django 1.11
* DjangoRestFramework 3.7

- - - -

### Setup Environment:
* Must have python3 installed
* Must have pip installed if not run from the shell in project directory to install:
    ```bash
       $ python3 get-pip.py
    ```
* To run project from single command please use the shell file on Debian like distribution:
    ```bash
        $ ./run.sh
    ```
* if not Debian follow the steps for the setup (although not Windows nor Mac):
    * install virtualenv using:
        ```bash
            $ pip install virtualenv
        ```
    * initialize and activate virtualenv using:
        ```bash
            $ virtualenv env
            $ source activate env/bin/activate
        ```
    * inside virtualenv install the requirements:
        ```bash
            $ pip install -r requirments.txt
        ```
    * generate database files using:
        ```bash
            $ python3 manage.py makemigrations
            $ python3 manage.py migrate
        ```
    * runserver on localhost and port 8000:
        ```bash
            $ python3 manage.py runserver
        ```

**Other OSes will not differ from those instructions but i could not make a generic file that create single command for every distribution but as DevOps this will not be hard.**