from django.contrib.auth import get_user_model
from rest_framework import permissions
from rest_framework.decorators import permission_classes, authentication_classes, parser_classes, api_view
from rest_framework.generics import CreateAPIView
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.views import APIView

from .authentication import TokenAuthentication
from .constants import FIRST_NAME, LAST_NAME, PHONES, ID, PHONE_NUMBER, FAIL_STATUS_CODE, MSG
from .models import Contact, Phone
from .serializers import UserSerializer, ContactRequestSerializer, AddPhoneRequestSerializer


@api_view(['POST'])
@authentication_classes((TokenAuthentication,))
@permission_classes((permissions.IsAuthenticated,))
@parser_classes((JSONParser,))
def add_phone(request, contacts_id):
	serializer = AddPhoneRequestSerializer(data=request.data)
	if not serializer.is_valid():
		response_data = {
			MSG: serializer.errors,
			
			}
		return Response(data=response_data)
	
	try:
		contact = Contact.objects.get(user_id=request.user.id, id=contacts_id)
	except:
		response_data = {
			MSG: "Contact does not exist",
			
			}
		return Response(data=response_data)
	
	try:
		Phone.objects.create(phone_number=request.data[PHONE_NUMBER], contact_id=contact)
		response_data = {
			ID: contact.id,
			}
		return Response(data=response_data)
	except Exception as e:
		print(e)
		response_data = {
			MSG: "adding failed",
			}
		return Response(status=FAIL_STATUS_CODE, data=response_data)


class CreateUserView(CreateAPIView):
	model = get_user_model()
	permission_classes = [
		permissions.AllowAny  # Or anon users can't register
		]
	
	serializer_class = UserSerializer


class ContactView(APIView):
	"""
	Create, Delete, Update any Contact, also Get list of Contacts
		* Requires token authentication.
	"""
	authentication_classes = (TokenAuthentication,)
	permission_classes = (permissions.IsAuthenticated,)
	parser_classes = (JSONParser,)
	
	def get(self, request, format=None):
		"""
		:return: list of related contacts to this user.
		"""
		contacts = Contact.objects.filter(user_id=request.user.id)
		response_data = []
		for contact in contacts:
			phones_query = Phone.objects.filter(contact_id=contact.id)
			phone_array = []
			for phone in phones_query:
				phone_array.append(phone.phone_number)
			contact_data = {
				ID: contact.id,
				FIRST_NAME: contact.first_name,
				LAST_NAME: contact.last_name,
				PHONES: phone_array,
				}
			response_data.append(contact_data)
		return Response(data=response_data)
	
	def post(self, request, format=None):
		"""
		Saves new Contact
		:param request: first_name, last_name
		:return: Contact id
		"""
		serializer = ContactRequestSerializer(data=request.data)
		if not serializer.is_valid():
			response_data = {
				MSG: serializer.errors.values(),
				
				}
			return Response(status=FAIL_STATUS_CODE, data=response_data)
		
		first_name = ""
		last_name = ""
		if FIRST_NAME in request.data:
			first_name = request.data[FIRST_NAME]
		if LAST_NAME in request.data:
			last_name = request.data[LAST_NAME]
		
		contact = Contact.objects.create(user_id=request.user, first_name=first_name, last_name=last_name)
		
		response_data = {
			"id": contact.id,
			}
		return Response(data=response_data)
	
	def delete(self, request, contacts_id, format=None):
		if contacts_id is None:
			response_data = {
				MSG: "Must be a valid contact id.",
				
				}
			return Response(status=FAIL_STATUS_CODE, data=response_data)
		try:
			contact = Contact.objects.get(id=contacts_id, user_id=request.user.id)
		except:
			response_data = {
				MSG: "Must be a valid contact id.",
				
				}
			return Response(status=FAIL_STATUS_CODE, data=response_data)
		
		contact.delete()
		return Response(data={})
	
	def put(self, request, format=None):
		serializer = ContactRequestSerializer(data=request.data)
		if not serializer.is_valid():
			response_data = {
				MSG: serializer.errors.values(),
				
				}
			return Response(status=FAIL_STATUS_CODE, data=response_data)
		
		if ID in request.data:
			try:
				contact = Contact.objects.get(id=request.data[ID], user_id=request.user.id)
				if FIRST_NAME in request.data:
					contact.first_name = request.data[FIRST_NAME]
				if LAST_NAME in request.data:
					contact.last_name = request.data[LAST_NAME]
				contact.save()
				response_data = {
					ID: contact.id,
					FIRST_NAME: contact.first_name,
					LAST_NAME: contact.last_name,
					}
				return Response(data=response_data)
			except:
				response_data = {
					MSG: "Contact does not exist or does not belong to the user.",
					
					}
				return Response(status=FAIL_STATUS_CODE, data=response_data)
		else:
			response_data = {
				MSG: "Contact does not exist or does not belong to the user.",
				
				}
			return Response(status=FAIL_STATUS_CODE, data=response_data)
